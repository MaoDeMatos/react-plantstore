# Introduction

Studying project, created with [Create React App](https://github.com/facebook/create-react-app), during my self-learning of [TailwindCSS](https://tailwindcss.com/) and [React](https://reactjs.org/) with this online course : [Start with React](https://openclassrooms.com/fr/courses/7008001-debutez-avec-react) (French).

It is a basic plant store.

## Start

Quick start the Docker container :

```sh
# In the app root dir
docker-compose up -d
```

Manually start with react scripts :

```sh
# In the app root dir
yarn && yarn start
```

:exclamation: Server at `localhost:5000` for **Docker** and `localhost:3000` for **manual start** :exclamation:

## Docker configuration

There are 2 Dockerfiles in the root dir :

- `Dockerfile-node` : serves the website from NodeJS, so the running stage uses the same image as the building stage.
- `Dockerfile` : lighter to run (<30 MB final image), but uses a different image to build the app and to run.

## Caveats

It uses the CSS property `backdrop-filter`, which is not supported by Firefox at the time of creation.
