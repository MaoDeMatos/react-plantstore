# Dependencies image
FROM node:16.13-alpine as deps

WORKDIR /app

COPY package.json yarn.lock ./
RUN yarn

# Building image
FROM node:16.13-alpine as builder

WORKDIR /app

COPY ./public       ./public
COPY ./src          ./src
COPY ./*config.js   ./
COPY --from=deps    /app        ./
RUN yarn run build

# Final image
FROM nginx:1.20-alpine as app

WORKDIR /app

COPY ./docker/nginx.conf  /etc/nginx/conf.d/default.conf
COPY --from=builder       /app/build        ./
