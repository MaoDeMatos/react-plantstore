import PlantItem from "./Plants/PlantItem";

export default function ShoppingList({
  plantList,
  cart,
  updateCart,
  currentCategory,
}) {
  function addToCart(name, price) {
    const currentPlantInCartIndex = cart.products.findIndex((plant) => plant.name === name);

    let cartNotification = cart.isDisplayed ? false : true;

    if (currentPlantInCartIndex !== -1) {
      const newCartProducts = cart.products;
      newCartProducts[currentPlantInCartIndex].amount += 1;
      updateCart({
        ...cart,
        hasNewItem: cartNotification,
        products: [
          ...newCartProducts
        ]
      });
    } else {
      updateCart({
        ...cart,
        hasNewItem: cartNotification,
        products: [
          ...cart.products,
          { name, price, amount: 1 }
        ]
      });
    }
  }

  return (
    <div
      id="shopping-list"
      className="flex flex-wrap items-start justify-center my-2"
    >
      {plantList.map(
        (plant) =>
          (currentCategory === "all" || currentCategory === plant.category) && (
            <PlantItem plant={plant} key={plant.id} addToCart={addToCart} />
          )
      )}
    </div>
  );
}
