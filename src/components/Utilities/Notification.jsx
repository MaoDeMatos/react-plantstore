export default function Notification() {
  return (
    <span className='absolute flex w-3 h-3 top-1 right-1'>
      <span className='absolute inline-flex w-3 h-3 rounded-full animate-ping bg-primary-400/80'></span>
      <span className='relative inline-flex w-3 h-3 transition rounded-full bg-primary-600 dark:bg-primary-500'></span>
    </span>
  );
}
