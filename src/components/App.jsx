import { useState, useEffect } from "react";

import MainFooter from "./MainFooter";
import MainHeader from "./MainHeader";
import MainPage from "./MainPage";

export default function App() {
  const brandName = "Plant Store";

  const defaultCart = {
    isDisplayed: false,
    hasNewItem: false,
    products: [],
  };

  const savedCart = localStorage.getItem("savedCart");

  const [cart, updateCart] = useState(savedCart ? JSON.parse(savedCart) : defaultCart);

  // Save current cart in localStorage
  useEffect(() => {
    if (cart.products.length > 0) {
      localStorage.setItem("savedCart", JSON.stringify(cart));
    } else localStorage.removeItem("savedCart");
  }, [cart]);

  return (
    <>
      <MainHeader brandName={brandName} />
      <MainPage cart={cart} updateCart={updateCart} />
      <MainFooter cart={cart} updateCart={updateCart} />
    </>
  );
}
