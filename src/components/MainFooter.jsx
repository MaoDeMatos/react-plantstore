import Nav from "./Navigation/Nav";

export default function MainFooter({ cart, updateCart }) {
  return (
    <footer className='fixed bottom-0 left-0 z-50 w-full h-16 transition duration-300 shadow-xl bg-neutral-200/90 text-neutral-800 dark:bg-neutral-500/90 dark:text-neutral-100 glass'>
      <div className='flex items-center justify-center h-full mx-auto italic lg:container'>
        <Nav cart={cart} updateCart={updateCart} />
      </div>
    </footer>
  );
}
