import Logo from "../Logo";
import Notification from "../Utilities/Notification";

export default function Nav({ cart, updateCart }) {
  function toggleCart() {
    if (cart.isDisplayed) {
      updateCart({ ...cart, isDisplayed: false });
    } else {
      updateCart({ ...cart, isDisplayed: true, hasNewItem: false });
    }
  }

  function toggleTheme() {
    const rootEl = document.documentElement;
    rootEl.classList.toggle("dark");
    if (rootEl.classList.contains("dark")) {
      localStorage.setItem("theme", "dark");
      // Change browser tab color when meta theme-color is supported
      // (Mobile browsers, Safari & Chrome with PWA at the time of creation)
      rootEl
        .querySelector('meta[name="theme-color"]')
        .setAttribute("content", "#737373");
    } else {
      localStorage.setItem("theme", "light");
      // Change browser tab color when meta theme-color is supported
      // (Mobile browsers, Safari & Chrome with PWA at the time of creation)
      rootEl
        .querySelector('meta[name="theme-color"]')
        .setAttribute("content", "#e5e5e5");
    }
  }

  const buttonClasses =
    "relative flex flex-col justify-center items-center p-2 max-h-full";
  const iconClasses = "block material-icons-outlined text-[2rem]";
  const labelClasses = "text-xs inline-block";

  return (
    <div className='flex items-center w-full main-nav justify-evenly'>
      <button type='button' className={buttonClasses + " disabled"}>
        <span className={iconClasses}>home</span>
        <span className={labelClasses}>Accueil</span>
      </button>

      <button type='button' className={buttonClasses + " disabled"}>
        <span className={iconClasses}>info</span>
        <span className={labelClasses}>Info</span>
      </button>

      <button type='button'>
        <Logo customClasses='h-12 relative left-1' />
      </button>

      <button
        type='button'
        className={buttonClasses}
        onClick={() => toggleTheme()}
      >
        <span className={iconClasses}>brightness_4</span>
        <span className={labelClasses}>Thème</span>
      </button>

      <button
        type='button'
        onClick={() => toggleCart()}
        className={buttonClasses}
      >
        {cart.hasNewItem && <Notification />}
        <span className={iconClasses}>shopping_cart</span>
        <span className={labelClasses}>Panier</span>
      </button>
    </div>
  );
}
