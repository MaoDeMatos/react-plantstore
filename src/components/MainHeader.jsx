import Logo from "../components/Logo";

export default function MainHeader({ brandName }) {
  return (
    <header
      id='main-header'
      className='fixed top-0 left-0 z-50 w-full h-16 transition duration-300 shadow-lg bg-neutral-200/90 text-neutral-800 dark:bg-neutral-500/90 dark:text-neutral-100 glass'
    >
      <div className='flex items-center justify-center h-full mx-auto lg:container sm:px-8'>
        <Logo customClasses='last:mr-6 first:ml-6 h-12' />
        <h1 className='last:mr-6 first:ml-6'>{brandName}</h1>
      </div>
    </header>
  );
}
