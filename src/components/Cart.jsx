import { useEffect } from "react";

import Button from "./Forms/Button";

export default function Cart({ cart, updateCart }) {
  const total = cart.products.reduce(
    (accumulation, plantType) =>
      accumulation + plantType.amount * plantType.price,
    0
  );

  function emptyCart() {
    updateCart({ ...cart, products: [] });
  }

  useEffect(() => {
    if (total) {
      document.title = `Plant Store: Panier: ${total}€`;
    } else document.title = "Plant Store";
  }, [total]);

  return (
    <>
      {cart.isDisplayed && (
        <div
          id='cart'
          className='transition duration-300 shadow-xl border border-primary-600 flex flex-col fixed z-10 w-[calc(100%_-_1rem)] px-4 py-3 rounded-lg left-2 bottom-24 sm:w-auto sm:left-auto sm:right-4 lg:right-1/2 lg:translate-x-[439px] max-h-[min(100%_-_12rem,_19.2rem)] bg-neutral-200/90 text-neutral-800 dark:bg-neutral-500/90 dark:text-neutral-100 glass'
        >
          <h2 className='my-2'>Panier</h2>

          {cart.products.length > 0 ? (
            <>
              <hr />
              <ul className='overflow-y-auto'>
                {cart.products.map(({ name, price, amount }, index) => (
                  <li key={`${name}-${index}`}>
                    {amount}x {name} ({price}€ l'unité)
                  </li>
                ))}
              </ul>

              <hr />
              <div className='flex items-center justify-between sm:flex-col'>
                <p className='text-xl text-center'>Total : {total}€</p>
                <Button
                  text='Vider le panier'
                  click={() => emptyCart()}
                  customClasses='my-2'
                />
              </div>
            </>
          ) : (
            <p>Panier vide !</p>
          )}
        </div>
      )}
    </>
  );
}
