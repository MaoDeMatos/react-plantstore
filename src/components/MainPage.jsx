import { useEffect, useState } from "react";

// import Form from "./Forms/Form";
import Cart from "./Cart";
import ShoppingList from "./ShoppingList";
import PlantCategories from "./Plants/PlantCategories";
import { plantList } from "../data/PlantList";

export default function MainPage({ cart, updateCart }) {
  const categories = plantList.reduce(
    (total, current) =>
      total.includes(current.category) ? total : total.concat(current.category),
    []
  );

  // States
  const [currentCategory, setCurrentCategory] = useState("all");
  const [currentCategoryListLength, setCurrentCategoryListLength] = useState(0);

  useEffect(() => {
    setCurrentCategoryListLength(
      currentCategory === "all"
        ? plantList.length
        : plantList.filter((plant) => plant.category === currentCategory).length
    );
  }, [currentCategory]);

  return (
    <div
      id='main-page'
      className='flex flex-col items-center pb-16 mt-16 lg:container'
    >
      <Cart cart={cart} updateCart={updateCart} />
      <PlantCategories
        categories={categories}
        currentCategory={currentCategory}
        setCurrentCategory={setCurrentCategory}
      />

      <p className='my-2'>
        {currentCategoryListLength === 1
          ? currentCategoryListLength + " résultat."
          : currentCategoryListLength + " résultats."}
      </p>

      <ShoppingList
        plantList={plantList}
        cart={cart}
        updateCart={updateCart}
        currentCategory={currentCategory}
      />
    </div>
  );
}
