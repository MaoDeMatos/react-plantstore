import logo from "../assets/logo.png";

export default function Logo({ customClasses }) {
  return (
    <img
      src={logo}
      alt='Logo'
      title='Logo'
      className={(customClasses + " logo aspect-square").trim()}
    />
  );
}
