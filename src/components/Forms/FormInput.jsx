import { useState } from "react";

import { firstCharToUpper } from "../../utils/utils";

export default function FormInput({
  name,
  label,
  type = "text",
  defaultValue,
}) {
  label = firstCharToUpper(label);
  defaultValue = label;

  const [inputValue, setInputValue] = useState(defaultValue);

  // function handleChange(e) {
  //   e.preventDefault();
  //   console.table(e);
  // }

  function emailVerification() {
    if (name === "email") {
      if (!inputValue.includes("@")) {
        console.error('Adresse email invalide ("@" manquant).');
      } else console.log("Adresse email valide.");
    }
  }

  return (
    <p className='w-full flex flex-col items-center p-2'>
      <label htmlFor={name} className='my-2'>
        {label}
      </label>
      <input
        id={name}
        type={type}
        name={name}
        value={inputValue}
        onChange={
          (e) => setInputValue(e.target.value)
          // handleChange(e)
        }
        onBlur={() => emailVerification()}
        className='transition-[outline] bg-neutral-600 text-neutral-200 p-2 rounded-lg outline outline-0 outline-primary-800 focus:outline-primary-600 focus:outline-4 hover:outline-4'
      />
    </p>
  );
}
