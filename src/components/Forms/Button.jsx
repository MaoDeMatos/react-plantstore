export default function Button({
  type = "button",
  color = "primary",
  text = "Default button text",
  click = null,
  customClasses = "",
  outline = false,
}) {
  // Add a space only if there are custom classes
  // if (customClasses) {
  //   customClasses = " " + customClasses;
  // }

  return (
    <button
      type={type}
      onClick={click}
      // prettier-ignore
      className={
        (
          "btn-" +
          color +
          (outline ? "-outline text-current " : " text-neutral-100 ") +
          " " +
          customClasses
        ).trim() // Remove spaces if no custom classes are given
      }
    >
      {text}
    </button>
  );
}
