import { useState } from "react";

import { toggleState } from "../../utils/utils";

import FormInput from "./FormInput";
import Button from "./Button";

export default function Form() {
  const [formIsDisplayed, setFormIsDisplayed] = useState(false);

  function resetForm(e) {
    e.preventDefault();
    document.getElementById("form-example").reset();
  }

  function submitForm(e) {
    e.preventDefault();
    console.log(e.target);
  }

  return (
    <form
      method='post'
      id='form-example'
      className='flex flex-col items-center p-4 mt-4 mb-2 w-max bg-neutral-700 rounded-2xl'
      onSubmit={submitForm}
    >
      <div className='flex items-center justify-between'>
        <h2 className='my-2 highlight'>Formulaire</h2>
        <Button
          color='primary'
          text='On/Off'
          click={() => toggleState(formIsDisplayed, setFormIsDisplayed)}
          customClasses='ml-4'
        />
      </div>
      {formIsDisplayed && (
        <>
          <FormInput name='first' label='prénom' />

          <FormInput name='last' label='nom' />

          <FormInput name='email' label='email' type='email' />

          <p className='flex justify-around w-full py-2'>
            <Button
              type='reset'
              color='rose'
              text='Reset'
              click={() => resetForm()}
            />
            <Button
              type='submit'
              color='primary'
              text='Valider'
              click={() => submitForm()}
            />
          </p>
        </>
      )}
    </form>
  );
}
