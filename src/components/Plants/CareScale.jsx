export default function CareScale({ scaleValue, careType }) {
  const range = [1, 2, 3];

  const typeIcon = careType === "light" ? "☀️" : "💧";

  return (
    <p>
      {range.map(
        (num) =>
          scaleValue >= num && <span key={num.toString()}>{typeIcon}</span>
      )}
    </p>
  );
}
