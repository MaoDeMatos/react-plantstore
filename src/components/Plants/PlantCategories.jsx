import { firstCharToUpper } from "../../utils/utils";

export default function PlantCategories({
  categories,
  currentCategory,
  setCurrentCategory,
}) {
  return (
    <div className='flex flex-col items-center justify-center mt-4 mb-2'>
      <h2 className='mb-2'>Trier par</h2>

      <select
        id='categories-list'
        name='Catégories'
        className='cursor-pointer btn-primary text-neutral-100'
        value={currentCategory}
        onChange={(e) => setCurrentCategory(e.target.value)}
      >
        <option value='all'>Toutes catégories</option>

        {categories.map((cat) => (
          <option value={cat} key={cat}>
            {firstCharToUpper(cat)}
          </option>
        ))}
      </select>
    </div>
  );
}
