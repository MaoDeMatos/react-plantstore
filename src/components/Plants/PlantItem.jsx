import Button from "../Forms/Button";
import CareScale from "./CareScale";

import { firstCharToUpper } from "../../utils/utils";

export default function PlantItem({ plant, addToCart }) {
  return (
    <div className='mx-4 my-2 sm:m-3 overflow-hidden capitalize shadow-xl rounded-2xl bg-primary-800 sm:max-w-[44%] lg:max-w-[27%] text-neutral-100'>
      <div className='flex justify-between w-full p-4 text-lg bg-primary-700'>
        <h2>{plant.name}</h2>
        <p>{plant.price}€</p>
      </div>

      <img
        src={plant.cover}
        alt={firstCharToUpper(plant.name) + " img"}
        title={firstCharToUpper(plant.name) + " img"}
        className='object-cover w-full select-none aspect-square'
      />

      <div className='w-full p-4'>
        <div className='flex items-center justify-around w-full mb-4'>
          <p className='text-center'>
            Catégorie : <br />
            {plant.category}
          </p>

          <div className='flex flex-col items-center justify-center'>
            <CareScale careType='light' scaleValue={plant.light} />
            <CareScale careType='water' scaleValue={plant.water} />
          </div>
        </div>

        <div className='flex w-full'>
          <Button
            click={() => addToCart(plant.name, plant.price)}
            text='Ajouter au panier'
            customClasses='mx-auto'
          />
        </div>
      </div>
    </div>
  );
}
