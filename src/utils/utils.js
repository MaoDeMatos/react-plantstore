export function firstCharToUpper(string) {
  return string.substring(0, 1).toUpperCase() + string.substring(1);
}

export function toggleState(s, set) {
  if (s) {
    set(false);
  } else set(true);
}
