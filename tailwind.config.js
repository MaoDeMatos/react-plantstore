const colors = require("tailwindcss/colors");

module.exports = {
  content: ["./src/**/*.{html,js*,css}", "./**/public/index.html"],
  darkMode: "class",
  theme: {
    colors: {
      transparent: "transparent",
      current: "currentColor",
      black: colors.black,
      white: colors.white,
      neutral: colors.neutral,
      rose: colors.rose,
      primary: colors.teal,
    },
    screens: {
      sm: "640px",
      md: "768px",
      lg: "1024px",
      // xl: "1280px",
      // "2xl": "1536px",
    },
    extend: {
      container: { center: true },
    },
  },
  plugins: [],
};
